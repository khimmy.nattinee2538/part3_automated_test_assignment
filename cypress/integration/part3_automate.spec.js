import AddProductToCartStep from '../stepDefinitions/addProductToCartStep';

const productfile1 = 'productData/product_1.json';
const productfile2 = 'productData/product_2.json';

describe('Part 3 : Automated test assignment .', () => {
	beforeEach(() => {
		AddProductToCartStep.customerGoToMyStore('/');
	});

	it('Customer can add 1 products and 1 item', () => {
		AddProductToCartStep.customerSearchProduct(productfile1);
		AddProductToCartStep.customerAddProductToCart();
		AddProductToCartStep.customerVerifyProductOnPopup(productfile1);
		AddProductToCartStep.customerClosePopUp();
		AddProductToCartStep.customerViewCart(productfile1);
	});

	it('Customer can add 1 products and 2 item', () => {
		AddProductToCartStep.customerSearchProduct(productfile1);
		AddProductToCartStep.customerAddProductToCart();
		AddProductToCartStep.customerVerifyProductOnPopup(productfile1);
		AddProductToCartStep.customerClosePopUp();

		AddProductToCartStep.customerSearchProduct(productfile1);
		AddProductToCartStep.customerAddProductToCart();
		AddProductToCartStep.customerVerifyProductOnPopup(productfile1);
		AddProductToCartStep.customerClosePopUp();
		AddProductToCartStep.customerViewCart(productfile1);
	});

	it('Customer can add many products', () => {
		AddProductToCartStep.customerSearchProduct(productfile1);
		AddProductToCartStep.customerAddProductToCart();
		AddProductToCartStep.customerVerifyProductOnPopup(productfile1);
		AddProductToCartStep.customerClosePopUp();

		AddProductToCartStep.customerSearchProduct(productfile2);
		AddProductToCartStep.customerAddProductToCart();
		AddProductToCartStep.customerVerifyAnyProductOnPopup(productfile2);
		AddProductToCartStep.customerClosePopUp();
		AddProductToCartStep.customerViewCartAnyProduct(productfile2);
	});

	it('Customer can delete product from cart', () => {
		AddProductToCartStep.customerSearchProduct(productfile1);
		AddProductToCartStep.customerAddProductToCart();
		AddProductToCartStep.customerVerifyProductOnPopup(productfile1);
		AddProductToCartStep.customerClosePopUp();
		AddProductToCartStep.customerViewCart(productfile1);
		AddProductToCartStep.customerDeleteProductFromCart();
		AddProductToCartStep.customerVerifyCartProductEmpty();
	});
});
