import myStorePage from '../pageObject/myStorePage';
import searchResultPage from '../pageObject/searchResultPage';
import {
	caltotalpricevalue,
	caltotalvalue,
	countproductquantity,
	productold,
	countanyproductquantity,
	calanytotalpricevalue,
	caculateProductPriceTotal,
	caculateTotal,
	countproduct,
	countAnyproduct,
	caculateAnyProductPriceTotal,
	clearCaculate,
} from '../function/calculateProduct';

class addProductToCartStep {
	static customerGoToMyStore(url) {
		myStorePage.goToMyStore(url);
		clearCaculate();
	}

	static customerSearchProduct(filename) {
		myStorePage.verifyTitlePage();
		cy.fixture(filename).then(data => {
			myStorePage.searchProduct(data.productname);
		});
	}

	static customerAddProductToCart() {
		searchResultPage.verifyTitlePage();
		searchResultPage.clickAddToCart();
	}

	static customerVerifyProductOnPopup(filename) {
		cy.fixture(filename).then(data => {
			countproduct(data.quantity);
			caculateProductPriceTotal(data.price);
			caculateTotal(data.shipping);

			searchResultPage.verifyAddToCartSuccess();
			searchResultPage.verifyproductname(data.productname);
			searchResultPage.verifyDetailProduct(data.productdetail);
			searchResultPage.verifyProductQuantity(countproductquantity);
			searchResultPage.verifyProductPriceTotal(caltotalpricevalue);
			searchResultPage.verifyTotalProduct(caltotalpricevalue);
			searchResultPage.verifyTotalShipping(data.shipping);
			searchResultPage.verifyTotal(caltotalvalue);
		});
	}

	static customerVerifyAnyProductOnPopup(filename) {
		cy.fixture(filename).then(data => {
			countproduct(data.quantity);
			caculateProductPriceTotal(data.price);
			caculateAnyProductPriceTotal(data.price, data.productname);
			caculateTotal(data.shipping);
			countAnyproduct(data.quantity, data.productname);

			searchResultPage.verifyAddToCartSuccess();
			searchResultPage.verifyproductname(data.productname);
			searchResultPage.verifyDetailProduct(data.productdetail);
			searchResultPage.verifyProductQuantity(countanyproductquantity);
			searchResultPage.verifyProductPriceTotal(calanytotalpricevalue);
			searchResultPage.verifyTotalProduct(caltotalpricevalue.toFixed(2));
			searchResultPage.verifyTotalShipping(data.shipping);
			searchResultPage.verifyTotal(caltotalvalue);
		});
	}

	static customerClosePopUp() {
		searchResultPage.closePopup();
	}

	static customerViewCart(filename) {
		cy.fixture(filename).then(data => {
			searchResultPage.verifyQuantityOnCartIcon(countproductquantity);
			searchResultPage.clickViewMyShoppingCart();
			searchResultPage.verifyCartProducNameAndQuantity(
				data.productname,
				countproductquantity
			);
			searchResultPage.verifyCartTotalPrice(caltotalpricevalue);
			searchResultPage.verifyCartShipping(data.shipping);
			searchResultPage.verifyCartTotal(caltotalvalue);
		});
	}

	static customerViewCartAnyProduct(filename) {
		cy.fixture(filename).then(data => {
			searchResultPage.verifyQuantityOnCartIcon(countproductquantity);
			searchResultPage.clickViewMyShoppingCart();
			searchResultPage.verifyCartProducNameAndQuantity(
				data.productname,
				countanyproductquantity
			);
			searchResultPage.verifyCartTotalPrice(caltotalpricevalue.toFixed(2));
			searchResultPage.verifyCartShipping(data.shipping);
			searchResultPage.verifyCartTotal(caltotalvalue);
		});
	}

	static customerDeleteProductFromCart() {
		searchResultPage.deteleProductFromCart();
	}

	static customerVerifyCartProductEmpty() {
		searchResultPage.verifyQuantityOnCartIconEmpty();
	}
}

export default addProductToCartStep;
