var caltotalpricevalue = 0;
var caltotalvalue = 0;
var countproductquantity = 0;
var productold = '';
var countanyproductquantity = 0;
var calanytotalpricevalue = 0;

function caculateProductPriceTotal(price) {
	caltotalpricevalue = caltotalpricevalue + price;
	return caltotalpricevalue;
}

function caculateAnyProductPriceTotal(price, productname) {
	if (productold == productname) {
		productold = productname;
		calanytotalpricevalue = calanytotalpricevalue + price;
	} else {
		calanytotalpricevalue = 0 + price;
	}
	return calanytotalpricevalue;
}

function caculateTotal(shipping) {
	caltotalvalue = shipping + caltotalpricevalue;
	caltotalvalue = Number(caltotalvalue.toFixed(2));
	return caltotalvalue;
}

function countproduct(quantity) {
	countproductquantity = countproductquantity + quantity;
	return countproductquantity;
}

function countAnyproduct(quantity, productname) {
	if (productold == productname) {
		productold = productname;
		countanyproductquantity = countanyproductquantity + quantity;
	} else {
		countanyproductquantity = 0 + quantity;
	}
	return countanyproductquantity;
}

function clearCaculate() {
	caltotalpricevalue = 0;
	caltotalvalue = 0;
	countproductquantity = 0;
	productold = '';
	countanyproductquantity = 0;
	calanytotalpricevalue = 0;
	return (
		caltotalpricevalue,
		caltotalvalue,
		countproductquantity,
		productold,
		countanyproductquantity,
		calanytotalpricevalue
	);
}

export {
	caltotalpricevalue,
	caltotalvalue,
	countproductquantity,
	productold,
	countanyproductquantity,
	calanytotalpricevalue,
	caculateProductPriceTotal,
	caculateTotal,
	countproduct,
	countAnyproduct,
	caculateAnyProductPriceTotal,
	clearCaculate,
};
