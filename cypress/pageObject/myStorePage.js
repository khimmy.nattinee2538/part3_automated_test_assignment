const TITLE_PAGE = 'My Store';
const myStorePage_titlelabel = 'head>title';
const myStorePage_inputsearch = '#search_query_top';

class myStorePage {
	static goToMyStore(url) {
		cy.visit(url);
	}
	static verifyTitlePage() {
		cy.get(myStorePage_titlelabel).should('contain.text', TITLE_PAGE);
	}

	static searchProduct(productname) {
		cy.get(myStorePage_inputsearch, { timeout: 2000 }).clear();
		cy.get(myStorePage_inputsearch, { timeout: 2000 }).type(
			productname + '{enter}'
		);
	}
}

export default myStorePage;
