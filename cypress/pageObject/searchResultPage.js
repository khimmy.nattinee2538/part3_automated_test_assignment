const TITLE_PAGE = 'Search - My Store';
const ADDSUCCESS_MSG = 'Product successfully added to your shopping cart';
const searchResultPage__titlelabel = 'head>title';
const searchResultPage_addtocartbutton = 'a.ajax_add_to_cart_button';
const searchResultPage_addtocardsuccesslabel = '#layer_cart';
const searchResultPage_productdetaillabel = '#layer_cart_product_attributes';
const searchResultPage_productquantitylabel = '#layer_cart_product_quantity';
const searchResultPage_productpricelabel = '#layer_cart_product_price';
const searchResultPage_producttotallabel = 'span.ajax_block_products_total';
const searchResultPage_totalshippinglabel = 'strong';
const searchResultPage_carttotalparentlabel = 'div.layer_cart_row';
const searchResultPage_totallabel = 'span.ajax_block_cart_total';
const searchResultPage_closepopupbutton = 'span[title="Close window"]';
const searchResultPage_cartquantitybutton = 'span.ajax_cart_quantity';
const searchResultPage_viewshoppingcartbutton =
	'a[title="View my shopping cart"]';
const searchResultPage_cartquantitylabel = 'span';
const searchResultPage_carttotalpricelabel = 'span.ajax_cart_total';
const searchResultPage_cartshippinglabel = 'div.cart-prices-line.first-line';
const searchResultPage_carttotallabel = 'div.cart-prices-line.last-line';
const searchResultPage_productnamelabel = '#layer_cart_product_title';
const searchResultPage_deletebutton = 'span[class="remove_link"]';
const searchResultPage_cartNoproduct = 'span.ajax_cart_no_product';

class searchResultPage {
	static verifyTitlePage() {
		cy.get(searchResultPage__titlelabel).should('contain.text', TITLE_PAGE);
	}

	static clickAddToCart() {
		cy.get(searchResultPage_addtocartbutton).eq(0).click();
	}

	static verifyAddToCartSuccess() {
		cy.get(searchResultPage_addtocardsuccesslabel, { timeout: 2000 }).should(
			'contain.text',
			ADDSUCCESS_MSG
		);
	}

	static verifyproductname(productname) {
		cy.get(searchResultPage_productnamelabel, { timeout: 20000 }).should(
			'contain.text',
			productname
		);
	}
	static verifyDetailProduct(productdetail) {
		cy.get(searchResultPage_productdetaillabel, { timeout: 20000 }).should(
			'contain.text',
			productdetail
		);
	}

	static verifyProductQuantity(quantity) {
		cy.get(searchResultPage_productquantitylabel).should(
			'contain.text',
			quantity
		);
	}

	static verifyProductPriceTotal(price) {
		cy.get(searchResultPage_productpricelabel).should(
			'contain.text',
			'$' + price
		);
	}

	static verifyTotalProduct(total) {
		cy.get(searchResultPage_producttotallabel).should(
			'contain.text',
			'$' + total
		);
	}

	static verifyTotalShipping(totalshipping) {
		cy.get(searchResultPage_totalshippinglabel)
			.contains('Total shipping')
			.next()
			.should('contain.text', '$' + totalshipping);
	}

	static verifyTotal(total) {
		cy.get(searchResultPage_carttotalparentlabel)
			.children(searchResultPage_totallabel)
			.should('contain.text', '$' + total);
	}

	static closePopup() {
		cy.get(searchResultPage_closepopupbutton).click();
	}

	static verifyQuantityOnCartIcon(quantity) {
		cy.scrollTo('top');
		cy.get('b')
			.next(searchResultPage_cartquantitybutton)
			.should('contain.text', quantity);
	}

	static clickViewMyShoppingCart() {
		cy.get(searchResultPage_viewshoppingcartbutton).trigger('mouseover');
	}

	static verifyCartProducNameAndQuantity(productname, quantity) {
		cy.get('a[title="' + productname + '"]')
			.parent('div')
			.children(searchResultPage_cartquantitylabel)
			.should('contain.text', quantity);
	}

	static verifyCartTotalPrice(price) {
		cy.get(searchResultPage_carttotalpricelabel).should(
			'contain.text',
			'$' + price
		);
	}

	static verifyCartShipping(shipping) {
		cy.get(searchResultPage_cartshippinglabel).should(
			'contain.text',
			'$' + shipping
		);
	}

	static verifyCartTotal(total) {
		cy.get(searchResultPage_carttotallabel).should('contain.text', '$' + total);
	}

	static deteleProductFromCart() {
		cy.scrollTo('top');
		cy.get(searchResultPage_deletebutton).click();
	}

	static verifyQuantityOnCartIconEmpty() {
		cy.scrollTo('top');
		cy.get(searchResultPage_cartNoproduct).should('contain.text', '(empty)');
	}
}

export default searchResultPage;
